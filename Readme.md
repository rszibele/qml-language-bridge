# qml-language-bridge

Simple program to allow any language which can spawn processes and has networking capabilities to interface with Qt QML. It is intended to be deployed alongside your application.

You are also allowed to statically link the Qt5 libraries into qml-language-bridge to provide a single binary `qmllb.exe` for deployment.

## Prerequisites

Ensure you have the Qt5 development files installed.

## Building

### Windows - Qt Creator

To bulid qml-lanuage-bridge you can use Qt Creator. Simply open the `CMakeLists.txt` file through the menu
`File->Open file or project...` and after opening the project click on the build icon.

### GNU/Linux

Execute the following commands in the root directory using the terminal of your choice:

````
$ cmake .
$ make
````
## Example programs

<a href="https://gitlab.com/rszibele/e-juice-calc#readme">e-juice-calc</a>
