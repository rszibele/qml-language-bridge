import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: main
    visible: true
    title: 'Simple test'
    width: 250
    height: 100

    GridLayout {
        columns: 2
        Label { text: 'Host: ' }
        TextField { text: qmllb.getHost() }
        Label { text: 'Port: ' }
        TextField { text: qmllb.getPort() }
    }
}
