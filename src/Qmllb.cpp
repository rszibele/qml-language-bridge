# include "Qmllb.h"

Q_INVOKABLE QString Qmllb::getHost() {
    return config->host;
}
Q_INVOKABLE int Qmllb::getPort() {
    return config->port;
}
Q_INVOKABLE void Qmllb::setWindowIcon(QWindow *window, QString filePath) {
    window->setIcon(QIcon(filePath));
}
