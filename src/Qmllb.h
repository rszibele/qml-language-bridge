#ifndef QMLLB_QMLLB_H
#define QMLLB_QMLLB_H

#include <QString>
#include <QObject>
#include <QWindow>
#include <QIcon>

#include "Configuration.h"

class Qmllb : public QObject {
    Q_OBJECT
public:
    Qmllb(Configuration *config, QObject *parent = 0) : QObject(parent) {
        this->config = config;
    }
    virtual ~Qmllb() {
    }
    Q_INVOKABLE QString getHost();
    Q_INVOKABLE int getPort();
    Q_INVOKABLE void setWindowIcon(QWindow *window, QString filePath);
private:
    Configuration *config;
};

#endif
