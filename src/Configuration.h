#ifndef QMLLB_CONFIGURATION_H
#define QMLLB_CONFIGURATION_H

#include <QString>

class Configuration {
public:
    Configuration() {
        host = "localhost";
        port = 0;
    }
    QString host;
    int port;
    QString file;
};

#endif
