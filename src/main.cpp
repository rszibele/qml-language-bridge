#include <QApplication>
#include <QCommandLineParser>
#include <QFileInfo>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QUrl>

#include "Qmllb.h"
#include "Configuration.h"

enum CommandLineParseResult {
    CommandLineOk,
    CommandLineError,
    CommandLineVersionRequested,
    CommandLineHelpRequested
};

CommandLineParseResult parseCommandLine(QCommandLineParser &parser,
    Configuration *config, QString *errorMessage);

int main(int argc, char **argv) {
    QApplication app(argc, argv);
    QCoreApplication::setApplicationName("qmllb");
    QCoreApplication::setApplicationVersion("1.0.3");

    // Configuration object.
    Configuration config;

    // Set up and parse the command line arguments.
    QString errorMessage;
    QCommandLineParser parser;
    CommandLineParseResult cmdResult = parseCommandLine(parser, &config, &errorMessage);

    switch(cmdResult) {
        case CommandLineOk:
            break;
        case CommandLineError:
            fputs(qPrintable(errorMessage), stderr);
            fputs("\n\n", stderr);
            fputs(qPrintable(parser.helpText()), stderr);
            return 1;
        case CommandLineVersionRequested:
            printf("%s %s\n", qPrintable(QCoreApplication::applicationName()),
                qPrintable(QCoreApplication::applicationVersion()));
            return 0;
        case CommandLineHelpRequested:
            parser.showHelp();
            return 0;
    }

    // create the object that we pass to QML.
    Qmllb *qmllb = new Qmllb(&config);

    // Create the ApplicationEngine and pass our object to it.
    QQmlApplicationEngine *appEngine = new QQmlApplicationEngine();
    QQmlEngine *engine = (QQmlEngine*)appEngine;
    engine->rootContext()->setContextProperty("qmllb", qmllb);

    // load specified file.
    appEngine->load(QUrl::fromLocalFile(config.file));
    
    // run the event loop of the application.
    int result = app.exec();

    // free our resources.
    delete appEngine;

    // return the result of our application.
    return result;
}

CommandLineParseResult parseCommandLine(QCommandLineParser &parser, Configuration *config, QString *errorMessage)
{
    parser.setApplicationDescription("Language independent bridge for displaying and communicating with QML.");
    const QCommandLineOption helpOption = parser.addHelpOption();
    const QCommandLineOption versionOption = parser.addVersionOption();
    parser.addPositionalArgument("port", QCoreApplication::translate("main", "Local port to listen on."));
    parser.addPositionalArgument("file", QCoreApplication::translate("main", "Main QML file to launch."));

    if(!parser.parse(QCoreApplication::arguments())) {
        *errorMessage = parser.errorText();
        return CommandLineError;
    }

    if (parser.isSet(versionOption))
        return CommandLineVersionRequested;

    if (parser.isSet(helpOption))
        return CommandLineHelpRequested;

    const QStringList positionalArguments = parser.positionalArguments();
    if (positionalArguments.isEmpty()) {
        *errorMessage = "Argument 'port' missing.";
        return CommandLineError;
    }
    if (positionalArguments.size() < 2) {
        *errorMessage = "Argument 'file' missing.";
        return CommandLineError;
    }
    if(positionalArguments.size() > 2) {
        *errorMessage = "Too many arguments.";
        return CommandLineError;
    }

    // Parse the port argument.
    const QString portStr = positionalArguments[0];
    bool portIsValid = false;
    config->port = portStr.toInt(&portIsValid);
    if(!portIsValid) {
        *errorMessage = "Argument 'port' must be a number.";
        return CommandLineError;
    }

    // Parse the file argument.
    const QString fileStr = positionalArguments[1];
    QFileInfo fileInfo(fileStr);
    if(!fileInfo.exists()) {
        *errorMessage = "The specified file does not exist.";
        return CommandLineError;
    }
    if(!fileInfo.isFile()) {
        *errorMessage = "The specified file is not a file.";
        return CommandLineError;
    }
    config->file = fileStr;

    return CommandLineOk;
}
